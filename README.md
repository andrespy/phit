# PHIT
phit makes working with git easier in our lights project
### Fast switch between YOUR branches
![](media/checkout.mp4)
### Create branches
![](media/branch_creation.png)

### Commit with no more gitlint problems
![](media/commit.mp4)

### Option navigation
![](media/options.mp4)

### Set up

Add to your $PROFILE the following function.

```
function phit{
C:\Users\apr7996\pythontest\Scripts\Activate.ps1
py C:\Users\apr7996\pythontest\phit\phit.py $args[0] $args[1]
deactivate
}
```