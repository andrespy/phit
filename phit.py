import sys
import os
import re
from git import *
import inquirer
import webbrowser
from inquirer.themes import GreenPassion
sys.path.append(os.path.realpath('.'))



class Phit:
    def __init__(self):
        try:
            self.repo = Repo('.', search_parent_directories=True)
        except:
            print("You're not on a git repo")
            exit(1)
        self.user = ""
        self.ticketPrefix = ['LCEI', 'BCP', 'CCP']
        self.cmds = ['commit', 'checkout', 'ticket', 'branch']

    def selectBranch(self):
        branches = self.findUsersBranches()
        branchQuestion = inquirer.List('branch',
                                       message="Select branch",
                                       choices=branches,
                                       ),
        selectedBranch = inquirer.prompt(branchQuestion, theme=GreenPassion()).get('branch')
        try:
            self.repo.git.checkout(selectedBranch)
        except GitCommandError as e:
            print('\033[91m'+ e.stderr[12:-1]+'\033[0m')
    def commit(self):
        if self.repo.active_branch.name == 'master':
            print('The project does not allow you to commit to the master branch')
            exit(1)
        changes = len(self.repo.index.diff("HEAD"))
        if changes == 0:
            print("No staged files found")
            exit(1)
        header = self.getCurrentBranchTicketName() + '/' + self.user + ': '
        message = header + input(header)
        if (len(message) > 100):
            message = message[:100]
        commitObj = self.repo.index.commit(message)
        print("Commited "+ commitObj.message + " to " +  self.repo.active_branch.name)
        if (changes > 1):
            print("["+str(changes) + " changes made]")
        else:
            print("["+ str(changes) + " change made]")


    def parseTicketName(self, rawTicket, hyphen=True, keepTail=False):
        hasPrefix = False
        for pref in self.ticketPrefix:
            i = rawTicket.upper().find(pref)
            if i != -1:
                index = i
                prefix = pref
                hasPrefix = True

        if (not hasPrefix):
            print("The valid prefixes are: " + ", ".join(self.ticketPrefix))
            exit(1)

        numeric = re.findall(r'\d+', rawTicket[index + len(prefix):])[0]
        if len(numeric) == 0:
            print("No numeric data found")
            exit(1)
        if (hyphen):
            ticketName = prefix + '-' + numeric
        else:
            ticketName = prefix + numeric
        if keepTail:
            ticketName += rawTicket[rawTicket.find(numeric) + len(numeric):]

        return ticketName

    def newBranch(self):
        if (len(sys.argv) < 3):
            ticket = input(
                "Type the name of the ticket (e.g. bcp1234-master, Lcei-7384_mydesc,CCP478): ")
        else:
            ticket = sys.argv[2]
        parsedTicket = self.parseTicketName(ticket, hyphen=False, keepTail=True)

        branchName = self.user + '-ticket-' + parsedTicket
        if branchName in self.findUsersBranches():
            print("Branch: " + branchName + " already exists")
            exit(1)
        self.repo.create_head(branchName)
        print("Created branch --> " + branchName)

    def findUsersBranches(self):
        branches = [h.name for h in self.repo.heads]
        myBranches = []
        for branch in branches:
            if (branch.find(self.user) != -1):
                myBranches.append(branch)
        myBranches.append('master')
        return myBranches

    def printHelp(self):
        print("Usage options:"
              "\n\tphit commit"
              "\n\tphit checkout"
              "\n\tphit ticket"
              "\n\tphit branch")

    def getCurrentBranchTicketName(self, hyphen=True):

        currentBranch = self.repo.active_branch.name
        if (currentBranch == 'master'):
            print('You are currently in the master branch. Therefore, there is no ticket associated to it')
            exit(1)
        for pref in self.ticketPrefix:
            index = currentBranch.find(pref)
            if (index != -1):
                if (hyphen):
                    return pref + '-' + str(re.split(r'\D+', currentBranch[index + len(pref):])[0])
                return pref + str(re.split(r'\D+', currentBranch[index + len(pref):])[0])
        print("Current branch does not contain a ticketname")
        exit(1)

    def openTicket(self):
        if (len(sys.argv) < 3):
            os.system("MicrosoftEdge.exe "+ 'https://asc.bwmgroup.net/jira/browse/' + self.getCurrentBranchTicketName())
            webbrowser.open('https://asc.bwmgroup.net/jira/browse/' + self.getCurrentBranchTicketName())
        else:
            os.system("MicrosoftEdge.exe " + 'https://asc.bwmgroup.net/jira/browse/' + self.parseTicketName(sys.argv[2]))
            webbrowser.open('https://asc.bwmgroup.net/jira/browse/' + self.parseTicketName(sys.argv[2]))

    def selectCommand(self):
        branchQuestion = inquirer.List('cmd',
                                       message="Select command",
                                       choices=self.cmds,
                                       ),
        return inquirer.prompt(branchQuestion, theme=GreenPassion()).get('cmd')

    def parseInputs(self, command: str):

        if command == self.cmds[0]:
            self.commit()
        elif command == self.cmds[1]:
            self.selectBranch()
        elif command == self.cmds[2]:
            self.openTicket()
        elif command == self.cmds[3]:
            self.newBranch()
        else:
            self.printHelp()

    def run(self):

        filename = "user.txt"
        filepath = os.path.dirname(os.path.realpath(__file__))
        filepath += filename
        try:
            file = open(filepath,'r')
        except:
            username = input("User not initialized, please introduce your username (e.g. Andres Prieto's username is AnPr):")
            file = open(filepath,'w')
            file.write(username)
            self.user = username
        else:
            self.user = file.readline()
        file.close()
        if len(sys.argv) == 1:
            self.parseInputs(self.selectCommand())
            exit(1)
        self.parseInputs(str(sys.argv[1]))


if __name__ == '__main__':
    try:
        phit = Phit()
        phit.run()
    except (KeyboardInterrupt, AttributeError):
        sys.exit(0)